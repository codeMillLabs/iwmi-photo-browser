/*
 * FILENAME
 *     ImageMetadata.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.photo.broswer.entity;

import static javax.persistence.GenerationType.AUTO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Entity class for keep the metadata on uploaded images.
 * </p>
 * 
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 * @version $Id$
 **/
@Entity
@Table(name = "IMG_META_DATA")
@NamedQueries(value = {
    @NamedQuery(name = "imageMetadata.findById", query = "SELECT i FROM ImageMetadata i WHERE i.id = :id")
})
public class ImageMetadata implements Serializable
{

    private static final long serialVersionUID = -3510528495987642016L;

    @Id
    @SequenceGenerator(name = "IMG_METADATA_SEQ", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = AUTO, generator = "IMG_METADATA_SEQ")
    @Column(name = "ID")
    private Long id;

    @Column(name = "IMAGE_ID")
    private String imageId;

    @Column(name = "IMAGE_NAME")
    private String imageName;

    @Column(name = "CATEGORY")
    private String category;

    @Column(name = "LONGITUDE")
    private double longitude;

    @Column(name = "LATITUDE")
    private double latitude;

    @Column(name = "ALTITUDE")
    private double altitude;

    @Column(name = "tilt")
    private double tilt;

    @ManyToOne
    private Subscriber owner;

    @Column(name = "IMG_PATH")
    private String imgPath;

    /**
     * <p>
     * Getter for imageId.
     * </p>
     * 
     * @return the imageId
     */
    public String getImageId()
    {
        return imageId;
    }

    /**
     * <p>
     * Setting value for imageId.
     * </p>
     * 
     * @param imageId
     *            the imageId to set
     */
    public void setImageId(String imageId)
    {
        this.imageId = imageId;
    }

    /**
     * <p>
     * Getter for imageName.
     * </p>
     * 
     * @return the imageName
     */
    public String getImageName()
    {
        return imageName;
    }

    /**
     * <p>
     * Setting value for imageName.
     * </p>
     * 
     * @param imageName
     *            the imageName to set
     */
    public void setImageName(String imageName)
    {
        this.imageName = imageName;
    }

    /**
     * <p>
     * Getter for category.
     * </p>
     * 
     * @return the category
     */
    public String getCategory()
    {
        return category;
    }

    /**
     * <p>
     * Setting value for category.
     * </p>
     * 
     * @param category
     *            the category to set
     */
    public void setCategory(String category)
    {
        this.category = category;
    }

    /**
     * <p>
     * Getter for longitude.
     * </p>
     * 
     * @return the longitude
     */
    public double getLongitude()
    {
        return longitude;
    }

    /**
     * <p>
     * Setting value for longitude.
     * </p>
     * 
     * @param longitude
     *            the longitude to set
     */
    public void setLongitude(double longitude)
    {
        this.longitude = longitude;
    }

    /**
     * <p>
     * Getter for latitude.
     * </p>
     * 
     * @return the latitude
     */
    public double getLatitute()
    {
        return latitude;
    }

    /**
     * <p>
     * Setting value for latitude.
     * </p>
     * 
     * @param latitude
     *            the latitute to set
     */
    public void setLatitude(double latitude)
    {
        this.latitude = latitude;
    }

    /**
     * <p>
     * Getter for altitude.
     * </p>
     * 
     * @return the altitude
     */
    public double getAltitude()
    {
        return altitude;
    }

    /**
     * <p>
     * Setting value for altitude.
     * </p>
     * 
     * @param altitude
     *            the altitude to set
     */
    public void setAltitude(double altitude)
    {
        this.altitude = altitude;
    }

    /**
     * <p>
     * Getter for tilt.
     * </p>
     * 
     * @return the tilt
     */
    public double getTilt()
    {
        return tilt;
    }

    /**
     * <p>
     * Setting value for tilt.
     * </p>
     * 
     * @param tilt
     *            the tilt to set
     */
    public void setTilt(double tilt)
    {
        this.tilt = tilt;
    }

    /**
     * <p>
     * Getter for id.
     * </p>
     * 
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * <p>
     * Getter for imgPath.
     * </p>
     * 
     * @return the imgPath
     */
    public String getImgPath()
    {
        return imgPath;
    }

    /**
     * <p>
     * Setting value for imgPath.
     * </p>
     * 
     * @param imgPath
     *            the imgPath to set
     */
    public void setImgPath(String imgPath)
    {
        this.imgPath = imgPath;
    }

    /**
     * <p>
     * Getter for owner.
     * </p>
     * 
     * @return the owner
     */
    public Subscriber getOwner()
    {
        return owner;
    }

    /**
     * <p>
     * Setting value for owner.
     * </p>
     * 
     * @param owner
     *            the owner to set
     */
    public void setOwner(Subscriber owner)
    {
        this.owner = owner;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String
            .format(
                "ImageMetadata [id=%s, imageId=%s, imageName=%s, category=%s, longitude=%s, latitude=%s, altitude=%s, tilt=%s, imgPath=%s]",
                id, imageId, imageName, category, longitude, latitude, altitude, tilt, imgPath);
    }

}

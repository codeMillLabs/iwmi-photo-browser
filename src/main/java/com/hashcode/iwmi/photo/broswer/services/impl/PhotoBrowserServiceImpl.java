/*
 * FILENAME
 *     PhotoBrowserServiceImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.iwmi.photo.broswer.services.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.hashcode.iwmi.photo.broswer.dao.ImageMetadataDao;
import com.hashcode.iwmi.photo.broswer.dao.ImageSequenceDao;
import com.hashcode.iwmi.photo.broswer.dao.SubscriberDao;
import com.hashcode.iwmi.photo.broswer.entity.ImageMetadata;
import com.hashcode.iwmi.photo.broswer.entity.Subscriber;
import com.hashcode.iwmi.photo.broswer.model.FieldImage;
import com.hashcode.iwmi.photo.broswer.model.UploadPayLoad;
import com.hashcode.iwmi.photo.broswer.services.PhotoBrowserService;
import com.hashcode.iwmi.photo.broswer.services.ServiceException;
import com.hashcode.iwmi.photo.broswer.util.Base64Converter;

//
//IMPORTS
//NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Photo browser service implementation.
 * </p>
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 * @version $Id$
 **/
@Service
@Transactional
public class PhotoBrowserServiceImpl implements PhotoBrowserService
{

    private static final Logger logger = LoggerFactory.getLogger(PhotoBrowserServiceImpl.class);

    @Autowired
    private ImageSequenceDao imageSequenceDao;

    @Autowired
    private ImageMetadataDao imageMetadataDao;

    @Autowired
    private SubscriberDao subscriberDao;

    @Value("${server.photo.repo.location}")
    private String imageRepoLocation;

    private static final String IMG_SEQ_CODE = "UPLOAD_IMG_SEQ";

    public List<FieldImage> convertPayload(UploadPayLoad payload) throws ServiceException
    {

        List<FieldImage> fieldImages = new ArrayList<FieldImage>(); //payload.getFieldImages();

        if (fieldImages.size() != payload.getNoOfImages())
        {
            throw new ServiceException("Error in Field image validation with payload meta data");
        }

        for (FieldImage image : fieldImages)
        {
            byte[] imageData = Base64Converter.getByte(image.getImageInB64());
            image.setImageData(imageData);
        }
        logger.info("Successfully converted the payload base 64 string data to image data");
        return fieldImages;
    }

    @Transactional
    public void saveUploadedImage(final MultipartFile imageFile, final UploadPayLoad payLoad) throws ServiceException
    {

        try
        {
            logger.info("Going to save the uploaded image, [ image : {}]", imageFile.getOriginalFilename());
            
            long nextValue = imageSequenceDao.getNextValue(IMG_SEQ_CODE);
            Subscriber subscriber = subscriberDao.getByUsername(payLoad.getUserName());

            String imageFileName = nextValue + "." + getFileExtention(imageFile);
            String imagePath = imageRepoLocation + File.pathSeparator + imageFileName;

            ImageMetadata metaData = new ImageMetadata();
            metaData.setOwner(subscriber);
            metaData.setCategory(payLoad.getFieldImage().getCategory());
            metaData.setImageId(String.valueOf(nextValue));
            metaData.setImageName(imageFileName);
            metaData.setImgPath(imagePath);

            metaData.setLatitude(payLoad.getFieldImage().getLatitude());
            metaData.setLongitude(payLoad.getFieldImage().getLongitude());
            metaData.setAltitude(payLoad.getFieldImage().getAltitude());
            metaData.setTilt(payLoad.getFieldImage().getTilt());
            

            imageMetadataDao.create(metaData);
            imageFile.transferTo(new File(imagePath));
            
            logger.info("Image saved successufully, [ image : {} ]", imageFileName);

        }
        catch (Exception e)
        {
            logger
                .error("Error encountered in saving the uploaded image, [" + imageFile.getOriginalFilename() + "]", e);
            throw new ServiceException("Error encountered in saving the uploaded image", e);
        }
    }

    private String getFileExtention(final MultipartFile imageFile)
    {
        String originalName = imageFile.getOriginalFilename();
        String extention = originalName.substring(originalName.lastIndexOf(".") + 1, originalName.length());
        return extention;
    }

    public static void main(String[] a)
    {

        String originalName = "amils.jpg";
        //        System.out.println(":: " + getFileExtention(originalName));
    }

}

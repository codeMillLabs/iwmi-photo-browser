/*
 * FILENAME
 *     TokenService.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.iwmi.photo.broswer.services;

/**
 * <p>
 * Token Validator Service.
 * </p>
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 * @version $Id$
 **/
public interface TokenService
{

    /**
     * <p>
     * Generates the token for the user and update the subscriber with new token.
     * </p>
     * 
     * @return
     * @throws ServiceException
     * 
     */
    String generateToken(String username) throws ServiceException;

    /**
     * <p>
     * Validate the each request with the token.
     * </p>
     * 
     * @param token
     *            token of the user
     * @param username
     *            username
     * @throws TokenValidationException
     *             throws at any validation error
     * 
     */
    void validateRequest(String token, String username) throws TokenValidationException;

}

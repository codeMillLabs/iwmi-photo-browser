/*
 * FILENAME
 *     MobileRequestController.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.iwmi.photo.broswer.util;

import org.apache.commons.codec.binary.Base64;

//
//IMPORTS
//NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Base 64 data converter utility class.
 * </p>
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 * @version $Id$
 **/
public final class Base64Converter
{

    /**
     * <p>
     * Convert Base 64 String to bytes.
     * </p>
     * 
     * @param base64String
     *            base 64 converted string
     * @return binary data in bytes;
     */
    public static byte[] getByte(final String base64String)
    {
        byte[] bytes = Base64.decodeBase64(base64String);

        return bytes;
    }

    /**
     * <p>
     * Convert Base 64 String to bytes.
     * </p>
     * 
     * @param binaryData
     *            binary data to be converted
     * @return binary data encoded in base 64.
     */
    public static String getInBase64(final byte[] binaryData)
    {
        String inBase64 = Base64.encodeBase64String(binaryData);

        return inBase64;
    }

}

/*
 * FILENAME
 *     TokenServiceImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.iwmi.photo.broswer.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hashcode.iwmi.photo.broswer.dao.SubscriberDao;
import com.hashcode.iwmi.photo.broswer.entity.Subscriber;
import com.hashcode.iwmi.photo.broswer.services.ServiceException;
import com.hashcode.iwmi.photo.broswer.services.TokenValidationException;
import com.hashcode.iwmi.photo.broswer.services.TokenService;
import com.hashcode.iwmi.photo.broswer.util.TokenGenerator;

//
//IMPORTS
//NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Token Service implementation.
 * </p>
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 * @version $Id$
 **/
@Service
@Transactional
public class TokenServiceImpl implements TokenService
{

    private static final Logger logger = LoggerFactory.getLogger(TokenServiceImpl.class);

    @Autowired
    private SubscriberDao subscriberDao;

    
    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.photo.broswer.services.TokenService#generateToken(java.lang.String)
     */
    public String generateToken(String username) throws ServiceException
    {
        Subscriber subscriber = subscriberDao.getByUsername(username);
        String token = TokenGenerator.generateToken(username);
        
        subscriber.setToken(token);
        subscriberDao.update(subscriber);
        
        return token;
    }


    /*
     * (non-Javadoc)
     * 
     * @see com.hashcode.iwmi.photo.broswer.services.TokenValidatorService# validateRequest(java.lang.String,
     * java.lang.String)
     */
    public void validateRequest(String token, String username) throws TokenValidationException
    {
        logger.debug("Going to validate the token from the request, [ Username : {} ]", username);

        Subscriber subscriber = subscriberDao.getByUsername(username);

        if (!subscriber.getToken().equals(token))
        {
            throw new TokenValidationException("Invalid User token");
        }
        else
        {
            logger.info("User token validation successfull, can proceed ...!");
        }
    }

}

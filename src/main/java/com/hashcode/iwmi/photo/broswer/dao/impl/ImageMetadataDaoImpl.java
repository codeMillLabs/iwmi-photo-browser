/*
 * FILENAME
 *     ImageMetadataDaoImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.photo.broswer.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hashcode.iwmi.photo.broswer.dao.ImageMetadataDao;
import com.hashcode.iwmi.photo.broswer.entity.ImageMetadata;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Implementation for the {@link UploadPayLoadDao}
 * </p>
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 * @version $Id$
 **/
public class ImageMetadataDaoImpl implements ImageMetadataDao
{

    private static final Logger logger = LoggerFactory.getLogger(ImageMetadataDaoImpl.class);

    @PersistenceContext(unitName = "com.hashcode.photoBrowser")
    private EntityManager entityManager;

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.iwmi.photo.broswer.dao.ImageMetadataDao#create(com.hashcode.iwmi.photo.broswer.entity.ImageMetadata)
     */
    public Long create(ImageMetadata metaData)
    {
        entityManager.persist(metaData);
        return metaData.getId();
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.iwmi.photo.broswer.dao.ImageMetadataDao#update(com.hashcode.iwmi.photo.broswer.entity.ImageMetadata)
     */
    public Long update(ImageMetadata metaData)
    {
        entityManager.persist(metaData);
        return metaData.getId();
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.iwmi.photo.broswer.dao.ImageMetadataDao#findMetaDataById(java.lang.Long)
     */
    public ImageMetadata findMetaDataById(Long id)
    {
        TypedQuery<ImageMetadata> query = entityManager.createNamedQuery("imageMetadata.findById", ImageMetadata.class);
        query.setParameter("id", id);
        query.setMaxResults(1);

        try
        {
            return query.getSingleResult();

        }
        catch (NoResultException e)
        {
            logger.info("No records found for given id : {}", id);
            return null;
        }
    }

}

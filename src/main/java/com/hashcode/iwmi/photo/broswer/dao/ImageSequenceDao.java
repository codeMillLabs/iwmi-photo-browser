/*
 * FILENAME
 *     ImageSequenceDao.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.photo.broswer.dao;

import java.util.List;

import com.hashcode.iwmi.photo.broswer.entity.ImageSequence;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * DAO class to maintain the image sequence;
 * </p>
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 * @version $Id$
 **/
public interface ImageSequenceDao
{

    /**
     * <p>
     * Create {@link ImageSequence}
     * </p>
     * 
     * @param imgSequence
     *            {@link ImageSequence}
     */
    void create(ImageSequence imgSequence);
    
    /**
     * <p>
     * Update {@link ImageSequence}
     * </p>
     * 
     * @param imgSequence
     *            {@link ImageSequence}
     */
    void update(ImageSequence imageSequence);

    /**
     * <p>
     * Retrieve {@link ImageSequence}
     * </p>
     * 
     * @param code
     *            code
     */
    ImageSequence getSequence(String code);
    
    /**
     * <p>
     * Retrieve the next value of the sequence and update the image sequenece;
     * </p>
     *
     * @param code sequence code
     * @return next sequence value;
     *
     */
    Long getNextValue(String code);

    /**
     * <p>
     * Retrieve all {@link ImageSequence}
     * </p>
     */
    List<ImageSequence> getAllSequences();
}

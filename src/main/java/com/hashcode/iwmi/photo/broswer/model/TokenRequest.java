/*
 * FILENAME
 *     TokenRequest.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.iwmi.photo.broswer.model;

import java.io.Serializable;

/**
 * <p>
 * TokenRequest model.
 * </p>
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 * @version $Id$
 **/
public class TokenRequest implements Serializable
{

    private static final long serialVersionUID = 5301814717440929132L;

    private String username;
    private String passcode;

    /**
     * @return the username
     */
    public String getUsername()
    {
        return username;
    }

    /**
     * @param username
     *            the username to set
     */
    public void setUsername(String username)
    {
        this.username = username;
    }

    /**
     * @return the passcode
     */
    public String getPasscode()
    {
        return passcode;
    }

    /**
     * @param passcode
     *            the passcode to set
     */
    public void setPasscode(String passcode)
    {
        this.passcode = passcode;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String.format("TokenRequest [username=%s, passcode=%s]", username, passcode);
    }

}

/*
 * FILENAME
 *     ResponseStatus.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.iwmi.photo.broswer.services;

/**
 * <p>
 * Enum for hold Response status messages.
 * </p>
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 * @version $Id$
 **/
public enum ResponseStatus
{

    SUCCESS("001", "Success"),
    FAILED("002", "Failed"),
    RETRY("003", "Retry"),
    AUTH_FAILED("004", "Authentication Failed");

    private String code;
    private String status;

    private ResponseStatus(java.lang.String code, String status)
    {
        this.code = code;
        this.status = status;
    }

    @Override
    public String toString()
    {
        return this.status;
    }

    public String getCode()
    {
        return code;
    }

    public String getStatus()
    {
        return status;
    }

}

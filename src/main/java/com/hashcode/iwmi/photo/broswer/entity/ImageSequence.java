/*
 * FILENAME
 *     ImageSequence.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.photo.broswer.entity;

import static javax.persistence.GenerationType.AUTO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Entity class for handle the Image Sequence.
 * </p>
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 * @version $Id$
 **/
@Entity
@Table(name = "IMG_SEQUENCE")
@NamedQueries(value = {
    @NamedQuery(name = "imageSequence.findByCode", query = "SELECT i FROM ImageSequence i WHERE i.code = :code"),
    @NamedQuery(name = "imageSequence.getAll", query = "SELECT i FROM ImageSequence i")
})
public class ImageSequence implements Serializable
{

    private static final long serialVersionUID = -2503693717681802816L;

    @Id
    @SequenceGenerator(name = "IMG_SEQ", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "IMG_SEQ", strategy = AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "CODE")
    private String code;

    @Column(name = "IMG_SEQ")
    private long imgSeq;

    @Version
    private long version;

    /**
     * <p>
     * Getter for imgSeq.
     * </p>
     * 
     * @return the imgSeq
     */
    public long getImgSeq()
    {
        return imgSeq;
    }

    /**
     * <p>
     * Setting value for imgSeq.
     * </p>
     * 
     * @param imgSeq
     *            the imgSeq to set
     */
    public void setImgSeq(long imgSeq)
    {
        this.imgSeq = imgSeq;
    }

    /**
     * <p>
     * Getter for code.
     * </p>
     * 
     * @return the code
     */
    public String getCode()
    {
        return code;
    }

    /**
     * <p>
     * Setting value for code.
     * </p>
     * 
     * @param code
     *            the code to set
     */
    public void setCode(String code)
    {
        this.code = code;
    }

    /**
     * <p>
     * Getter for id.
     * </p>
     * 
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String.format("ImageSequence [id=%s, code=%s, imgSeq=%s, version=%s]", id, code, imgSeq, version);
    }

}

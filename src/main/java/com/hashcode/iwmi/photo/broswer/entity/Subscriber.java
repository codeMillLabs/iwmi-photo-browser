/*
 * FILENAME
 *     Subscriber.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.photo.broswer.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Entity class for the app subscriber.
 * </p>
 * 
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 * @version $Id$
 **/
@Entity
@Table(name = "APP_SUBSCRIBER")
@NamedQueries(value = {
    @NamedQuery(name = "subscriber.findByUsername", query = "SELECT s FROM Subscriber s WHERE s.username = :username")
})
public class Subscriber implements Serializable
{

    private static final long serialVersionUID = -5050652977174381510L;

    @Id
    @SequenceGenerator(name = "SUBS_SEQ", allocationSize = 1, initialValue = 1000)
    @GeneratedValue(generator = "SUBS_SEQ", strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private long id;

    @Column(name = "USERNAME", unique = true)
    private String username;

    @Column(name = "PASSCODE")
    private String passcode;

    @Column(name = "TOKEN")
    private String token;

    @Column(name = "CRETAED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @Column(name = "LASR_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    @PrePersist
    public void beforeSave()
    {
        this.createdDate = Calendar.getInstance().getTime();
        this.lastModifiedDate = Calendar.getInstance().getTime();
    }

    @PreUpdate
    public void beforeUpdate()
    {
        this.lastModifiedDate = Calendar.getInstance().getTime();
    }

    /**
     * <p>
     * Getter for username.
     * </p>
     * 
     * @return the username
     */
    public String getUsername()
    {
        return username;
    }

    /**
     * <p>
     * Setting value for username.
     * </p>
     * 
     * @param username
     *            the username to set
     */
    public void setUsername(String username)
    {
        this.username = username;
    }

    /**
     * <p>
     * Getter for passcode.
     * </p>
     * 
     * @return the passcode
     */
    public String getPasscode()
    {
        return passcode;
    }

    /**
     * <p>
     * Setting value for passcode.
     * </p>
     * 
     * @param passcode
     *            the passcode to set
     */
    public void setPasscode(String passcode)
    {
        this.passcode = passcode;
    }

    /**
     * <p>
     * Getter for token.
     * </p>
     * 
     * @return the token
     */
    public String getToken()
    {
        return token;
    }

    /**
     * <p>
     * Setting value for token.
     * </p>
     * 
     * @param token
     *            the token to set
     */
    public void setToken(String token)
    {
        this.token = token;
    }

    /**
     * <p>
     * Getter for createdDate.
     * </p>
     * 
     * @return the createdDate
     */
    public Date getCreatedDate()
    {
        return createdDate;
    }

    /**
     * <p>
     * Setting value for createdDate.
     * </p>
     * 
     * @param createdDate
     *            the createdDate to set
     */
    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    /**
     * <p>
     * Getter for lastModifiedDate.
     * </p>
     * 
     * @return the lastModifiedDate
     */
    public Date getLastModifiedDate()
    {
        return lastModifiedDate;
    }

    /**
     * <p>
     * Setting value for lastModifiedDate.
     * </p>
     * 
     * @param lastModifiedDate
     *            the lastModifiedDate to set
     */
    public void setLastModifiedDate(Date lastModifiedDate)
    {
        this.lastModifiedDate = lastModifiedDate;
    }

    /**
     * <p>
     * Getter for id.
     * </p>
     * 
     * @return the id
     */
    public long getId()
    {
        return id;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String.format(
            "Subscriber [id=%s, username=%s, passcode=%s, token=%s, createdDate=%s, lastModifiedDate=%s]", id,
            username, passcode, token, createdDate, lastModifiedDate);
    }

}

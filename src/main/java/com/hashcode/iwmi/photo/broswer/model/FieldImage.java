/*
 * FILENAME
 *     FieldImage.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.iwmi.photo.broswer.model;

import java.io.Serializable;
import java.util.Arrays;

import javax.persistence.Transient;

/**
 * <p>
 * Field image model class.
 * </p>
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 * @version $Id$
 **/
public class FieldImage implements Serializable
{

    private static final long serialVersionUID = -1530050609631880364L;

    private String imageId;
    private String imageName;
    private String category;
    private double longitude;
    private double latitude;
    private double altitude;
    private double tilt;

    private String imageInB64;

    @Transient
    private byte[] imageData;

    /**
     * <p>
     * Getter for imageId.
     * </p>
     * 
     * @return the imageId
     */
    public String getImageId()
    {
        return imageId;
    }

    /**
     * <p>
     * Setting value for imageId.
     * </p>
     * 
     * @param imageId
     *            the imageId to set
     */
    public void setImageId(String imageId)
    {
        this.imageId = imageId;
    }

    /**
     * <p>
     * Getter for imageName.
     * </p>
     * 
     * @return the imageName
     */
    public String getImageName()
    {
        return imageName;
    }

    /**
     * <p>
     * Setting value for imageName.
     * </p>
     * 
     * @param imageName
     *            the imageName to set
     */
    public void setImageName(String imageName)
    {
        this.imageName = imageName;
    }

    /**
     * <p>
     * Getter for category.
     * </p>
     * 
     * @return the category
     */
    public String getCategory()
    {
        return category;
    }

    /**
     * <p>
     * Setting value for category.
     * </p>
     * 
     * @param category
     *            the category to set
     */
    public void setCategory(String category)
    {
        this.category = category;
    }

    /**
     * <p>
     * Getter for longitude.
     * </p>
     * 
     * @return the longitude
     */
    public double getLongitude()
    {
        return longitude;
    }

    /**
     * <p>
     * Setting value for longitude.
     * </p>
     * 
     * @param longitute
     *            the longitute to set
     */
    public void setLongitude(double longitude)
    {
        this.longitude = longitude;
    }
   
    /**
     * <p>
     * Getter for latitude.
     * </p>
     * 
     * @return the latitude
     */
    public double getLatitude()
    {
        return latitude;
    }

    /**
     * <p>
     * Setting value for latitude.
     * </p>
     * 
     * @param latitude the latitude to set
     */
    public void setLatitude(double latitude)
    {
        this.latitude = latitude;
    }

    /**
     * <p>
     * Getter for altitude.
     * </p>
     * 
     * @return the altitude
     */
    public double getAltitude()
    {
        return altitude;
    }

    /**
     * <p>
     * Setting value for altitude.
     * </p>
     * 
     * @param altitude the altitude to set
     */
    public void setAltitude(double altitude)
    {
        this.altitude = altitude;
    }

    /**
     * <p>
     * Getter for tilt.
     * </p>
     * 
     * @return the tilt
     */
    public double getTilt()
    {
        return tilt;
    }

    /**
     * <p>
     * Setting value for tilt.
     * </p>
     * 
     * @param tilt
     *            the tilt to set
     */
    public void setTilt(double tilt)
    {
        this.tilt = tilt;
    }

    /**
     * <p>
     * Getter for imageInB64.
     * </p>
     * 
     * @return the imageInB64
     */
    public String getImageInB64()
    {
        return imageInB64;
    }

    /**
     * <p>
     * Setting value for imageInB64.
     * </p>
     * 
     * @param imageInB64
     *            the imageInB64 to set
     */
    public void setImageInB64(String imageInB64)
    {
        this.imageInB64 = imageInB64;
    }

    /**
     * <p>
     * Getter for imageData.
     * </p>
     * 
     * @return the imageData
     */
    public byte[] getImageData()
    {
        return imageData;
    }

    /**
     * <p>
     * Setting value for imageData.
     * </p>
     * 
     * @param imageData
     *            the imageData to set
     */
    public void setImageData(byte[] imageData)
    {
        this.imageData = imageData;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String
            .format(
                "FieldImage [imageId=%s, imageName=%s, category=%s, longitude=%s, latitude=%s, altitude=%s, tilt=%s, imageInB64=%s, imageData=%s]",
                imageId, imageName, category, longitude, latitude, altitude, tilt, imageInB64,
                Arrays.toString(imageData));
    }

}

/*
 * FILENAME
 *     TokenGenerator.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.iwmi.photo.broswer.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//
//IMPORTS
//NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Token generator utility class.
 * </p>
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 * @version $Id$
 **/
public final class TokenGenerator
{

    private static final Logger logger = LoggerFactory.getLogger(TokenGenerator.class);

    /**
     * <p>
     * Generate the Token for the given phrase.
     * </p>
     * 
     * @param phrase
     *            text phrase to generate token
     * @return token
     */
    public static String generateToken(String phrase)
    {

        String salt = SaltPhraseGenerator.getSalt();

        String txtToMd5 = phrase + salt;

        String token = MD5Generator.getMD5String(txtToMd5);

        logger.info("Token Generated :  [ Request Code : {}, Token : {}]", phrase, token);
        return token;
    }

    public static void main(String[] ar)
    {
        System.out.println(":: " + TokenGenerator.generateToken("amila silva"));
    }

}

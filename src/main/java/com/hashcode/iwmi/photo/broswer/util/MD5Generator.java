/*
 * FILENAME
 *     MD5Generator.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.photo.broswer.util;

import java.security.MessageDigest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * MD5Generator class
 * </p>
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 * @version 1.0
 * 
 */
public final class MD5Generator
{

    private static final Logger logger = LoggerFactory.getLogger(MD5Generator.class);

    public static String getMD5String(String value)
    {
        try
        {
            if (value == null)
            {
                return null;
            }
            else
            {

                MessageDigest md = MessageDigest.getInstance("MD5");
                md.update(value.getBytes());

                byte byteData[] = md.digest();

                // convert the byte to hex format method 1
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < byteData.length; i++)
                {
                    sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
                }

                return sb.toString();
            }
        }
        catch (Exception e)
        {
            logger.error("Error occurred in generating the MD5 from string,", e);
        }
        return null;
    }

}

/*
 * FILENAME
 *     ImageSequenceDaoImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.photo.broswer.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.hashcode.iwmi.photo.broswer.dao.ImageSequenceDao;
import com.hashcode.iwmi.photo.broswer.entity.ImageSequence;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Implementation for the {@link ImageSequenceDao}
 * </p>
 * 
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 * @version $Id$
 **/
@Repository
public class ImageSequenceDaoImpl implements ImageSequenceDao
{

    private static final Logger logger = LoggerFactory.getLogger(ImageSequenceDaoImpl.class);

    @PersistenceContext(unitName = "com.hashcode.photoBrowser")
    private EntityManager entityManager;

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.iwmi.photo.broswer.dao.ImageSequenceDao#create(com.hashcode.iwmi.photo.broswer.entity.ImageSequence)
     */
    public void create(ImageSequence imgSequence)
    {
        entityManager.persist(imgSequence);
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.iwmi.photo.broswer.dao.ImageSequenceDao#update(com.hashcode.iwmi.photo.broswer.entity.ImageSequence)
     */
    public void update(ImageSequence imageSequence)
    {
        entityManager.merge(imageSequence);
    }
    
    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.photo.broswer.dao.ImageSequenceDao#getNextValue(java.lang.String)
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Long getNextValue(String code)
    {
        ImageSequence imgSequence = null;
        synchronized (this)
        {
            imgSequence = getSequence(code);
            entityManager.lock(imgSequence, LockModeType.PESSIMISTIC_FORCE_INCREMENT);
            imgSequence.setImgSeq(imgSequence.getImgSeq() + 1);
            update(imgSequence);
        }

        return imgSequence.getImgSeq();
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.iwmi.photo.broswer.dao.ImageSequenceDao#getSequence(java.lang.String)
     */
    public ImageSequence getSequence(String code)
    {
        TypedQuery<ImageSequence> query = entityManager.createNamedQuery("imageSequence.findByCode", ImageSequence.class);
        query.setParameter("code", code);
        query.setMaxResults(1);

        try
        {
            return query.getSingleResult();

        }
        catch (NoResultException e)
        {
            logger.info("No records found for code : {}", code);
            return null;
        }

    }

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.iwmi.photo.broswer.dao.ImageSequenceDao#getAllSequences()
     */
    public List<ImageSequence> getAllSequences()
    {
        TypedQuery<ImageSequence> query = entityManager.createNamedQuery("imageSequence.getAll", ImageSequence.class);
        return query.getResultList();
    }

}

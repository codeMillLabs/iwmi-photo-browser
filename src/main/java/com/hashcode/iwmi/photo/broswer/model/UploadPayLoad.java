/*
 * FILENAME
 *     UploadPayLoad.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.iwmi.photo.broswer.model;

import java.io.Serializable;

/**
 * <p>
 * Upload Pay Load model class to handle jason string.
 * </p>
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 * @version $Id$
 **/
public class UploadPayLoad implements Serializable
{

    private static final long serialVersionUID = 8158175207523577924L;

    private int noOfImages;
    private String userName;
    private String token;
    private FieldImage fieldImage;

    /**
     * @return the noOfImages
     */
    public int getNoOfImages()
    {
        return noOfImages;
    }

    /**
     * @param noOfImages
     *            the noOfImages to set
     */
    public void setNoOfImages(int noOfImages)
    {
        this.noOfImages = noOfImages;
    }

    /**
     * @return the token
     */
    public String getToken()
    {
        return token;
    }

    /**
     * @param token
     *            the token to set
     */
    public void setToken(String token)
    {
        this.token = token;
    }
    
    /**
     * <p>
     * Getter for fieldImage.
     * </p>
     * 
     * @return the fieldImage
     */
    public FieldImage getFieldImage()
    {
        return fieldImage;
    }

    /**
     * <p>
     * Setting value for fieldImage.
     * </p>
     * 
     * @param fieldImage the fieldImage to set
     */
    public void setFieldImage(FieldImage fieldImage)
    {
        this.fieldImage = fieldImage;
    }

    /**
     * @return the userName
     */
    public String getUserName()
    {
        return userName;
    }

    /**
     * @param userName
     *            the userName to set
     */
    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String.format("UploadPayLoad [noOfImages=%s, token=%s, fieldImage=%s]", noOfImages, token, fieldImage);
    }

}

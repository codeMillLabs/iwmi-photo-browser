/*
 * FILENAME
 *     MobileRequestController.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.iwmi.photo.broswer.controllers;

import static com.hashcode.iwmi.photo.broswer.services.ResponseStatus.AUTH_FAILED;
import static com.hashcode.iwmi.photo.broswer.services.ResponseStatus.FAILED;
import static com.hashcode.iwmi.photo.broswer.services.ResponseStatus.SUCCESS;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.hashcode.iwmi.photo.broswer.model.Response;
import com.hashcode.iwmi.photo.broswer.model.TokenRequest;
import com.hashcode.iwmi.photo.broswer.model.UploadPayLoad;
import com.hashcode.iwmi.photo.broswer.services.PhotoBrowserService;
import com.hashcode.iwmi.photo.broswer.services.SubscriberService;
import com.hashcode.iwmi.photo.broswer.services.SubscriberValidationException;
import com.hashcode.iwmi.photo.broswer.services.TokenService;
import com.hashcode.iwmi.photo.broswer.services.TokenValidationException;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Controller class for handle the all the mobile requests.
 * </p>
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 * @version $Id$
 **/
@Controller
public class MobileRequestController
{

    private static final Logger logger = LoggerFactory.getLogger(MobileRequestController.class);

    @Autowired
    private PhotoBrowserService photoBrowserService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private SubscriberService subscriberService;

    /**
     * 
     * <p>
     * Handles the single uploaded images and send back the response to client.
     * 
     * This will validate the request against the user token before start processing.
     * </p>
     * 
     * @param jsonRequest
     *            json data string contains metadata related to upload
     * @param imageFile
     *            uploaded the image file
     * @return response to the client
     * 
     */
    @RequestMapping(value = "/upload/img", method = {
        RequestMethod.POST, RequestMethod.PUT
    }, consumes = {
        "multipart/mixed"
    }, produces = "applcation/json")
    public @ResponseBody
    Response handleUploadedImage(@RequestPart(required = true) String jsonRequest,
        @RequestPart("imageFile") MultipartFile imageFile)
    {

        logger.info("Upload multipart request came to handle.");
        Response response = null;
        ObjectMapper mapper = new ObjectMapper();
        try
        {
            UploadPayLoad payLoad = mapper.readValue(jsonRequest, UploadPayLoad.class);

            tokenService.validateRequest(payLoad.getToken(), payLoad.getUserName());

            response = validateImageFile(imageFile);
            
            if (response != null)
                return response;

            logger.debug("Uploaded file received, [ Filename : {}, Size : {}]", imageFile.getOriginalFilename(),
                (imageFile.getSize() / 1024) + "KB");

            photoBrowserService.saveUploadedImage(imageFile, payLoad);
            response = new Response(SUCCESS, imageFile.getOriginalFilename());

            logger.info("Upload request successfully handled");
        }
        catch (TokenValidationException e)
        {
            logger.error("Error encountered in validating the request", e);
            response = new Response(AUTH_FAILED, "User Authentication Failed, Please login before use.");
        }
        catch (Exception e)
        {
            logger.error("Error encountered in handling the uploaded images", e);
            response =
                new Response(FAILED, "Failed to upload the image [" + imageFile.getOriginalFilename()
                    + "], Please Retry after sometime.");
        }
        return response;
    }

    private Response validateImageFile(MultipartFile imageFile)
    {
        Response response = null;
        if (imageFile == null || imageFile.getSize() < 1)
        {
            response = new Response(FAILED, "Error in uploaded the image, Please Retry after sometime.");
        }
        else if (!imageFile.getContentType().equals("image/jpeg"))
        {
            response =
                new Response(FAILED, "Failed to uploaded images in given format, [" + imageFile.getContentType() + "].");
        }
        return response;
    }

    /**
     * <p>
     * Generate the token for clients. This will validate the user credentials and generate the token to proceed with
     * system transactions.
     * </p>
     * 
     * @param jsonRequest
     *            jsone request
     * @return response as json.
     * 
     */
    @RequestMapping(value = "/genToken", method = {
        RequestMethod.POST, RequestMethod.PUT
    })
    public @ResponseBody
    Response generateUserToken(String jsonRequest)
    {
        logger.info("Request came to generate token for user");
        Response response;
        ObjectMapper mapper = new ObjectMapper();
        try
        {
            TokenRequest payLoad = mapper.readValue(jsonRequest, TokenRequest.class);

            subscriberService.validateUser(payLoad.getUsername(), payLoad.getPasscode());

            String token = tokenService.generateToken(payLoad.getUsername());

            response = new Response(SUCCESS, token);
            logger.info("Token genrated successfully and send to user. [ User : {}, Token :{}] ",
                payLoad.getUsername(), token);
        }
        catch (SubscriberValidationException e)
        {
            logger.error("Error encountered in Subcriber validation", e);
            response = new Response(FAILED, "Failed to authenticate user, Please Retry with correct credentials.");
        }
        catch (Exception e)
        {
            logger.error("Error encountered in generating user token ", e);
            response = new Response(FAILED, "Failed to Generate user token, Please Retry after sometime.");
        }

        return response;
    }

}

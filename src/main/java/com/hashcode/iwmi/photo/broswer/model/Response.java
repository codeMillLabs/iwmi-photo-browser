/*
 * FILENAME
 *     UploadResponse.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.iwmi.photo.broswer.model;

import java.io.Serializable;

import com.hashcode.iwmi.photo.broswer.services.ResponseStatus;

/**
 * <p>
 * Response model.
 * </p>
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 * @version $Id$
 **/
public class Response implements Serializable
{

    private static final long serialVersionUID = -8101074798817760516L;

    private String statusCode;
    private String status;
    private String message;

    public Response()
    {
        super();
    }

    public Response(ResponseStatus status, String message)
    {
        super();
        this.statusCode = status.getCode();
        this.status = status.getStatus();
        this.message = message;
    }

    public String getStatusCode()
    {
        return statusCode;
    }

    public void setStatusCode(String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String.format("Response [statusCode=%s, status=%s, message=%s]", statusCode, status, message);
    }

}

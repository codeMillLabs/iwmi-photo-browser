/*
 * FILENAME
 *     SubscriberService.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.photo.broswer.services;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Subscriber related functionalities handles here.
 * </p>
 * 
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 * @version $Id$
 **/
public interface SubscriberService
{
    /**
     * 
     * <p>
     * Check user exists in the system.
     * </p>
     * 
     * @param username
     *            username
     * @return true if user exists otherwise false
     * 
     */
    boolean checkUserExist(String username);

    /**
     * 
     * <p>
     * Validate the user credentials.
     * </p>
     * 
     * 
     * @param username
     *            user name of the user
     * @param passcode
     *            passcode of the user
     * @throws SubscriberValidationException
     *             throws at any error
     */
    void validateUser(String username, String passcode) throws SubscriberValidationException;

}

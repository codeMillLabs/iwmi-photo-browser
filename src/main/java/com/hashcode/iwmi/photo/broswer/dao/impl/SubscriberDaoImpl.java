/*
 * FILENAME
 *     SubscriberDaoImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.photo.broswer.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hashcode.iwmi.photo.broswer.dao.SubscriberDao;
import com.hashcode.iwmi.photo.broswer.entity.Subscriber;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Implementation for the {@link SubscriberDao}
 * </p>
 * 
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 * @version $Id$
 **/
@Repository
public class SubscriberDaoImpl implements SubscriberDao
{

    private static final Logger logger = LoggerFactory.getLogger(SubscriberDaoImpl.class);

    @PersistenceContext(unitName = "com.hashcode.photoBrowser")
    private EntityManager entityManager;

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.iwmi.photo.broswer.dao.SubscriberDao#create(com.hashcode.iwmi.photo.broswer.entity.Subscriber)
     */
    public void create(Subscriber subscriber)
    {
        entityManager.persist(subscriber);
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.iwmi.photo.broswer.dao.SubscriberDao#update(com.hashcode.iwmi.photo.broswer.entity.Subscriber)
     */
    public void update(Subscriber subscriber)
    {
        entityManager.merge(subscriber);
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.iwmi.photo.broswer.dao.SubscriberDao#getByUsername(java.lang.String)
     */
    @Transactional(readOnly = true)
    public Subscriber getByUsername(String username)
    {
        TypedQuery<Subscriber> query = entityManager.createNamedQuery("subscriber.findByUsername", Subscriber.class);
        query.setParameter("username", username);
        query.setMaxResults(1);

        try
        {
            return query.getSingleResult();

        }
        catch (NoResultException e)
        {
            logger.info("No records found for username : {}", username);
            return null;
        }
    }

}

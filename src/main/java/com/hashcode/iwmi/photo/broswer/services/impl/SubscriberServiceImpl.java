/*
 * FILENAME
 *     SubscriberServiceImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.photo.broswer.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.hashcode.iwmi.photo.broswer.dao.SubscriberDao;
import com.hashcode.iwmi.photo.broswer.entity.Subscriber;
import com.hashcode.iwmi.photo.broswer.services.SubscriberService;
import com.hashcode.iwmi.photo.broswer.services.SubscriberValidationException;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Implementation for the {@link SubscriberService}
 * </p>
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 * @version $Id$
 **/
public class SubscriberServiceImpl implements SubscriberService
{

    private static final Logger logger = LoggerFactory.getLogger(TokenServiceImpl.class);

    @Autowired
    private SubscriberDao subscriberDao;

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.iwmi.photo.broswer.services.SubscriberService#checkUserExist(java.lang.String)
     */
    public boolean checkUserExist(String username)
    {
        Subscriber subscriber = subscriberDao.getByUsername(username);
        if (subscriber == null)
            return false;
        else
            return true;

    }

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.iwmi.photo.broswer.services.SubscriberService#validateUser(java.lang.String, java.lang.String)
     */
    public void validateUser(String username, String passcode) throws SubscriberValidationException
    {
        Subscriber subscriber = subscriberDao.getByUsername(username);

        if (subscriber == null || !subscriber.getPasscode().equals(passcode))
        {
            throw new SubscriberValidationException("Subscriber validation failed");
        }
        else
        {
            logger.info("Subscriber username & password based authentication successfull");
        }

    }

}

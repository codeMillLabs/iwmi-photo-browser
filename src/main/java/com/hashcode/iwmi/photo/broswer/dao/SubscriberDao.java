/*
 * FILENAME
 *     SubscriberDao.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.photo.broswer.dao;

import com.hashcode.iwmi.photo.broswer.entity.Subscriber;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * DAO for {@link Subscriber}
 * </p>
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 * @version $Id$
 **/
public interface SubscriberDao
{

    /**
     * <p>
     * Create {@link Subscriber}
     * </p>
     * 
     * @param subscriber
     *            {@link Subscriber}
     */
    void create(Subscriber subscriber);

    /**
     * <p>
     * Update {@link Subscriber}
     * </p>
     * 
     * @param subscriber
     *            {@link Subscriber}
     */
    void update(Subscriber subscriber);

    /**
     * <p>
     * Retrieve {@link Subscriber} by username
     * </p>
     * 
     * @param username
     *            username
     */
    Subscriber getByUsername(String username);

}

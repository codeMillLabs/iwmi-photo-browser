/*
 * FILENAME
 *     PhotoBrowserService.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.photo.broswer.services;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.hashcode.iwmi.photo.broswer.model.FieldImage;
import com.hashcode.iwmi.photo.broswer.model.UploadPayLoad;

//
//IMPORTS
//NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Photo browser services.
 * </p>
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 * @version $Id$
 **/
public interface PhotoBrowserService
{

    /**
     * <p>
     * This will convert the base 64 String image to bytes and put in to Field Image.
     * </p>
     * 
     * 
     * @param payload
     *            uploaded payload
     * @return List of {@link FieldImage}
     * @throws ServiceException
     *             throws at any error
     */
    List<FieldImage> convertPayload(UploadPayLoad payload) throws ServiceException;

    /**
     * <p>
     * Save uploaded image in server.
     * 
     * This will enter the metadata to database and saved the file in the given location.
     * </p>
     * 
     * @param imageFile
     *            uploaded multipart file
     * @param payLoad
     *            metadata on upload
     * @throws ServiceException
     *             throws at any error
     * 
     */
    void saveUploadedImage(final MultipartFile imageFile, final UploadPayLoad payLoad) throws ServiceException;

}

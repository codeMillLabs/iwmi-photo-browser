/*
 * FILENAME
 *     SubscriberValidationException.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.photo.broswer.services;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Subscriber validation exception.
 * </p>
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 * @version $Id$
 **/
public class SubscriberValidationException extends Exception
{

    private static final long serialVersionUID = 3356860406574443187L;

    public SubscriberValidationException()
    {
        super();
    }

    public SubscriberValidationException(String message, Throwable cause, boolean enableSuppression,
        boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public SubscriberValidationException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public SubscriberValidationException(String message)
    {
        super(message);
    }

    public SubscriberValidationException(Throwable cause)
    {
        super(cause);
    }

}

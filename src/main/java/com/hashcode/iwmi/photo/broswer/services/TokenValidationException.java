/*
 * FILENAME
 *     TokenValidationException.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.iwmi.photo.broswer.services;

/**
 * <p>
 * Token Validation Exception
 * </p>
 * 
 * @author Amila Silva
 * @email amila@hashcodesys.com
 * @version $Id$
 **/
public class TokenValidationException extends Exception
{

    private static final long serialVersionUID = -8395957779977493642L;

    public TokenValidationException()
    {
        super();
    }

    public TokenValidationException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public TokenValidationException(String message)
    {
        super(message);
    }

    public TokenValidationException(Throwable cause)
    {
        super(cause);
    }

}
